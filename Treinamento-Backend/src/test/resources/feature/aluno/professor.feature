Feature: Teste de Professor

  Scenario: Incluir um professor
    Given estar na pagina principal para teste de professor
    When clicar em cadastrar no menu professor
    When Preencher os seguintes campos do professor
      | id                  | tipo        | valor           |
      | nomeProfessor          | Text        | professor cucumber  |
      | emailProfessor       | Text        | ola@google.com  |
      | cpfProfessor                  |Text        | 233244          |
      | datanascimentoProfessor | datepicker  | 22/12/1996      |
      | sexoProfessor           | select      | Masculino       |
    | biografia               | Text        | Bio             |
    When clicar no botao cadastrar professor
    Then verificar mensagem professor
    Then fecha browser professor

  Scenario: Erro ao incluir um professor
    Given estar na pagina principal para teste de professor
    When clicar em cadastrar no menu professor
    When Preencher os seguintes campos do professor
      | id                      | tipo        | valor               |
      | nomeProfessor           | Text        | professor cucumber  |
      | emailProfessor          | Text        | ola@google.com      |
      | cpfProfessor            |Text         | 233245              |
      | datanascimentoProfessor | datepicker  | 22/12/1996          |
      | sexoProfessor           | select      | Masculino           |
      | biografia               | Text        | Bio                 |
      | logradouroProfessor     | Text        | teste               |
    When clicar no botao cadastrar professor
    Then verificar se ocorreu mensagem de erro de professor
    Then fecha browser professor

  Scenario: Pesquisa de Professor
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    Then fecha browser professor

  Scenario:update professor
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    When clicar para fazer update do professor
    Then estar na tela de update do professor
    When Preencher os seguintes campos do professor
      | id                      | tipo        | valor           |
      | numeroProfessor         | Text        | 887             |
      | ufProfessor             | Text        | sp              |
      | bairroProfessor         | Text        | bairro          |
      | cepProfessor            | Text        | 134             |
      | logradouroProfessor     | Text        | teste           |
      | cidadeProfessor         |Text         | cidade          |
    When clicar no botao update do professor
    Then mensagem de sucesso do update professor
    Then fecha browser professor

  Scenario: Matricular Professor
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    When clicar para fazer update do professor
    Then estar na tela de update do professor
    When seleciona disciplina Teste-AA101
    When clicar em matricular
    Then mensagem de sucesso de matricula
    Then professor deve estar matriculado na disciplina com codigo AA101
    Then fecha browser professor

  Scenario: Excluir com erro
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    When clicar em excluir professor
    Then a mensagem de erro deve ser exibida
    Then fecha browser professor

  Scenario: Desmatricular Professor
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    When clicar para fazer update do professor
    Then estar na tela de update do professor
    When clicar para desmatricular a disciplina com o codigo AA101
    Then mensagem de sucesso da desmatricula
    Then professor nao deve estar matriculado na disciplina com codigo AA101
    Then fecha browser professor

  Scenario: Excluir
    Given estar na pagina principal para teste de professor
    When clicar em pesquisar menu professor
    When buscar pelo nome do professor professor cucumber
    Then deve retornar somente um professor
    When clicar em excluir professor
    Then a lista de professor deve estar vazia
    Then fecha browser professor
