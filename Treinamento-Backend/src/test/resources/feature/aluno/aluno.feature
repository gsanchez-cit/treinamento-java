Feature: Testes de aluno

  Scenario: Incluir um aluno
    Given estar na pagina principal
    When clicar em cadastrar no menu aluno
    When Preencher os seguintes campos
    | id                  | tipo        | valor           |
    | nomeAluno           | Text        | teste cucumber  |
    | emailAluno          | Text        | ola@google.com  |
    | cpfAluno            | Text        | 233244          |
    | datanascimentoAluno | datepicker  | 22/12/1996      |
    | sexoAluno           | select      | Masculino       |
    When clicar no botao cadastrar
    Then verificar mensagem
    Then fecha browser

  Scenario: Erro ao incluir um aluno
    Given estar na pagina principal
    When clicar em cadastrar no menu aluno
    When Preencher os seguintes campos
      | id                  | tipo        | valor           |
      | nomeAluno           | Text        | teste cucumber  |
      | emailAluno          | Text        | ola@google.com  |
      | cpfAluno            | Text        | 233245          |
      | datanascimentoAluno | datepicker  | 22/12/1996      |
      | sexoAluno           | select      | Masculino       |
      | logradouroAluno     | Text        | teste           |
    When clicar no botao cadastrar
    Then verificar se ocorreu mensagem de erro
    Then fecha browser

  Scenario: Pesquisa
    Given estar na pagina principal
    When clicar em pesquisar menu aluno
    When buscar pelo nome do aluno teste cucumber
    Then deve retornar somente um elemento
    Then fecha browser

  Scenario:
    Given estar na pagina principal
    When clicar em pesquisar menu aluno
    When buscar pelo nome do aluno teste cucumber
    Then deve retornar somente um elemento
    When clicar para fazer update
    Then estar na tela de update
    When Preencher os seguintes campos
      | id                  | tipo        | valor           |
      | numeroAluno         | Text        | 887             |
      | ufAluno             | Text        | sp              |
      | bairroAluno         | Text        | bairro          |
      | cepAluno            | Text        | 134             |
      | logradouroAluno     | Text        | teste           |
      | cidadeAluno         |Text         | cidade          |
    When clicar no botao update
    Then mensagemm de sucesso
    Then fecha browser

  Scenario: Excluir
    Given estar na pagina principal
    When clicar em pesquisar menu aluno
    When buscar pelo nome do aluno teste cucumber
    Then deve retornar somente um elemento
    When clicar em excluir
    Then a lista deve estar vazia
    Then fecha browser



