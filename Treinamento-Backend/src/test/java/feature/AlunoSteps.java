package feature;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by estagiocit on 01/03/2017.
 */
public class AlunoSteps {

    private WebDriver webDriver;

    @Given("^estar na pagina principal$")
    public void estarPaginaPrincipal() throws Throwable{
        webDriver = Conectores.iniciaNavgador();
        webDriver.navigate().to("localhost:3000");

    }



    @When("^entro nas paginas$")
    public void entroNasPaginas(List<Map<String, String>> values) throws Throwable {
        System.out.println(values);
        for(Map<String,String> elemento : values){
            webDriver.navigate().to(elemento.get("pagina"));
            System.out.println("pagina: "+elemento.get("numero"));
        }
    }

    @When("^clicar em cadastrar no menu aluno$")
    public void clicarEmCadastrarNoMenuAluno() throws Throwable {
        webDriver.findElement(By.id("menuAluno")).click();
        webDriver.findElement(By.id("CadastarMenuAluno")).click();
    }

    @When("^Preencher os seguintes campos$")
    public void preencherOsSeguintesCampos(List<Map<String,String>> valores) throws Throwable {
        for(Map<String, String> valor : valores){
            switch(valor.get("tipo")){
                case "Text": {
                    webDriver.findElement(By.id(valor.get("id"))).sendKeys(valor.get("valor"));
                    break;
                }
                case "select": {
                    Select select = new Select(webDriver.findElement(By.id(valor.get("id"))));
                    select.selectByVisibleText(valor.get("valor"));
                    break;
                }
                case "datepicker": {
                    webDriver.findElement(By.id(valor.get("id"))).sendKeys(valor.get("valor"));
                }
            }
        }
    }

    @Then("^esperar$")
    public void esperar() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^clicar no botao cadastrar$")
    public void clicarNoBotaoCadastrar() throws Throwable {
        webDriver.findElement(By.id("cadastrarAluno")).click();
    }

    @Then("^verificar mensagem$")
    public void verificarMensagem() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                                            Conditions.EXPECT_NO_SPINNERS,
                                            Conditions.EXPECT_NOT_WAITING,
                                            Conditions.EXPECT_RESPONSE);
            WebElement element = webDriver.findElement(By.id("result"));
            Assert.assertTrue(element.getText().equals("Item gravado com sucesso"));

    }

    @Then("^verificar se ocorreu mensagem de erro$")
    public void verificarSeOcorreuMensagemDeErro() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE);
        WebElement element = webDriver.findElement(By.id("result"));

        Assert.assertTrue(element.getText().equals("Erro ao gravar Item Endereco inválido"));
    }

    @When("^clicar em pesquisar menu aluno$")
    public void clicarEmPesquisarMenuAluno() throws Throwable {
        webDriver.findElement(By.id("menuAluno")).click();
        webDriver.findElement(By.id("BuscarMenuAluno")).click();

    }

    @When("^buscar pelo nome do aluno ([^0-9]*)$")
    public void buscarPeloNomeTesteCucumber(String nome) throws Throwable {
        webDriver.findElement(By.id("nomeAluno")).sendKeys(nome);
        webDriver.findElement(By.id("consultar")).click();

    }

    @Then("^deve retornar somente um elemento$")
    public void deveRetornarSomenteUmElemento() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE);
        WebElement element = webDriver.findElement(By.id("linkstable"));
        Assert.assertTrue(element.findElements(By.tagName("tr")).size() > 1);
    }

    @When("^clicar em excluir$")
    public void clicarEmExcluir() throws Throwable {
        webDriver.findElement(By.id("linkstable")).findElements(By.tagName("tr")).get(1).findElements(By.tagName("a")).get(1).click();
    }

    @Then("^a lista deve estar vazia$")
    public void aListaDeveEstarVazia() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE);
        WebElement element = webDriver.findElement(By.id("linkstable"));
        Assert.assertTrue(element.findElements(By.tagName("tr")).size() == 1);
    }

    @Then("^fecha browser$")
    public void fechaBrowser() throws Throwable {
        webDriver.close();
    }

    @When("^clicar para fazer update$")
    public void clicarParaFazerUpdate() throws Throwable {
        webDriver.findElement(By.id("linkstable")).findElements(By.tagName("tr")).get(1).findElements(By.tagName("a")).get(0).click();
    }

    @Then("^estar na tela de update$")
    public void estarNaTelaDeUpdate() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE);
    }

    @When("^clicar no botao update$")
    public void clicarNoBotaoUpdate() throws Throwable {
        webDriver.findElement(By.id("updateAluno")).click();
    }

    @Then("^mensagemm de sucesso$")
    public void mensagemmDeSucesso() throws Throwable {
        Utils.waitForPageLoad(30,webDriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE);
        WebElement element = webDriver.findElement(By.id("result"));
        Assert.assertTrue(element.getText().equals("Item gravado com sucesso"));

    }
}
