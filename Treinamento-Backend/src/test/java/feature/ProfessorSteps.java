package feature;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by estagiocit on 02/03/2017.
 */
public class ProfessorSteps {

    private WebDriver webdriver;

    @When("^clicar em cadastrar no menu professor$")
    public void clicarEmCadastrarNoMenuProfessor() throws Throwable {
        webdriver.findElement(By.id("menuProfessor")).click();
        webdriver.findElement(By.id("CadastarMenuProfessor")).click();
    }

    @Given("^estar na pagina principal para teste de professor$")
    public void estarNaPaginaPrincipalParaTesteDeProfessor() throws Throwable {
        webdriver = Conectores.iniciaNavgador();
        webdriver.navigate().to("http://localhost:3000");
    }

    @When("^Preencher os seguintes campos do professor$")
    public void preencherOsSeguintesCamposDoProfessor(List<Map<String, String>> valores) throws Throwable {
        for(Map<String, String> valor : valores){
            switch(valor.get("tipo")){
                case "Text": {
                    webdriver.findElement(By.id(valor.get("id"))).sendKeys(valor.get("valor"));
                    break;
                }
                case "select": {
                    Select select = new Select(webdriver.findElement(By.id(valor.get("id"))));
                    select.selectByVisibleText(valor.get("valor"));
                    break;
                }
                case "datepicker": {
                    webdriver.findElement(By.id(valor.get("id"))).sendKeys(valor.get("valor"));
                }
            }
        }
    }

    @When("^clicar no botao cadastrar professor$")
    public void clicarNoBotaoCadastrarProfessor() throws Throwable {
        webdriver.findElement(By.id("cadastrarProfessor")).click();
    }

    @Then("^verificar mensagem professor$")
    public void verificarMensagemProfessor() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_PROFESSOR);
        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("Professor salvo com sucesso"));
    }

    @Then("^fecha browser professor$")
    public void fechaBrowserProfessor() throws Throwable {
        webdriver.close();
    }

    @Then("^verificar se ocorreu mensagem de erro de professor$")
    public void verificarSeOcorreuMensagemDeErroDeProfessor() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_PROFESSOR);

        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("Erro ao gravar Professor: Endereco inválido"));

    }

    @When("^clicar em pesquisar menu professor$")
    public void clicarEmPesquisarMenuProfessor() throws Throwable {
        webdriver.findElement(By.id("menuProfessor")).click();
        webdriver.findElement(By.id("BuscarMenuProfessor")).click();
    }

    @When("^buscar pelo nome do professor ([^0-9]*)$")
    public void buscarPeloNomeTesteCucumber(String nome) throws Throwable {
        webdriver.findElement(By.id("nomeProfessor")).sendKeys(nome);
        webdriver.findElement(By.id("consultar")).click();

    }

    @Then("^deve retornar somente um professor$")
    public void deveRetornarSomenteUmProfessor() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_TABLE);
        WebElement element = webdriver.findElement(By.id("linkstable"));
        Assert.assertTrue(element.findElements(By.tagName("tr")).size() > 1);
    }

    @When("^clicar para fazer update do professor$")
    public void clicarParaFazerUpdateDoProfessor() throws Throwable {
        WebElement element = webdriver.findElement(By.id("linkstable"));
        element.findElements(By.tagName("tr")).get(1).findElements(By.tagName("a")).get(0).click();
    }
    @Then("^estar na tela de update do professor$")
    public void estarNaTelaDeUpdateDoProfessor() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_URL_UPDATE_PROFESSOR);
    }

    @When("^clicar no botao update do professor$")
    public void clicarNoBotaoUpdateDoProfessor() throws Throwable {
        webdriver.findElement(By.id("updateProfessor")).click();
    }

    @Then("^mensagem de sucesso do update professor$")
    public void mensagemDeSucessoDoUpdateProfessor() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_PROFESSOR);

        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("Professor atualizado com sucesso"));
    }

    @When("^clicar em excluir professor$")
    public void clicarEmExcluirProfessor() throws Throwable {
        WebElement element = webdriver.findElement(By.id("linkstable"));
        element.findElements(By.tagName("tr")).get(1).findElements(By.tagName("a")).get(1).click();
    }

    @Then("^a lista de professor deve estar vazia$")
    public void aListaDeProfessorDeveEstarVazia() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_PROFESSOR);

        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("Professor removido com sucesso."));
    }

    @When("^seleciona disciplina (.*)$")
    public void selecionaDisciplinaTesteAA(String disciplina) throws Throwable {
        Select select = new Select(webdriver.findElement(By.id("disciplinasMatricular")));
        select.selectByVisibleText(disciplina);
    }

    @When("^clicar em matricular$")
    public void clicarEmMatricular() throws Throwable {
        webdriver.findElement(By.id("matriculaProfessor")).click();
    }


    @Then("^mensagem de sucesso de matricula$")
    public void mensagemDeSucessoDeMatricula() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_DISCIPLINA);

        WebElement element = webdriver.findElement(By.id("resultDisciplina"));
        Assert.assertTrue(element.getText().equals("professor matriculado com sucesso"));
    }

    @Then("^professor deve estar matriculado na disciplina com codigo (.*)$")
    public void professorDeveEstarMatriculadoNaDisciplinaTesteAA(String disciplina) throws Throwable {
        WebElement webElement = webdriver.findElement(By.id("linkstable"));
        List<WebElement> elements = webElement.findElements(By.tagName("tr"));
        elements.remove(0);
        for (WebElement element: elements) {
            if(element.findElements(By.tagName("td")).get(1).getText().equals(disciplina)){
                Assert.assertTrue(true);
                return;
            }
        }
        Assert.fail();
    }

    @When("^clicar para desmatricular a disciplina com o codigo (.*)$")
    public void clicarParaDesmatricularADisciplinaComOCodigoAA(String disciplina) throws Throwable {
        WebElement webElement = webdriver.findElement(By.id("linkstable"));
        List<WebElement> elements = webElement.findElements(By.tagName("tr"));
        elements.remove(0);
        for (WebElement element: elements) {
            if(element.findElements(By.tagName("td")).get(1).getText().equals(disciplina)){
                element.findElements(By.tagName("a")).get(0).click();
                return;
            }
        }
        Assert.fail();
    }

    @Then("^mensagem de sucesso da desmatricula$")
    public void mensagemDeSucessoDaDesmatricula() throws Throwable {
        Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
                Conditions.EXPECT_NO_SPINNERS,
                Conditions.EXPECT_NOT_WAITING,
                Conditions.EXPECT_RESPONSE_PROFESSOR);

        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("professor desmatriculado com sucesso"));
    }

    @Then("^professor nao deve estar matriculado na disciplina com codigo (.*)$")
    public void professorNaoDeveEstarMatriculadoNaDisciplinaComCodigoAA(String disciplina) throws Throwable {
        WebElement webElement = webdriver.findElement(By.id("linkstable"));
        List<WebElement> elements = webElement.findElements(By.tagName("tr"));
        elements.remove(0);
        for (WebElement element: elements) {
            if(element.findElements(By.tagName("td")).get(1).getText().equals(disciplina)){
                Assert.fail();
                return;
            }
        }
        Assert.assertTrue(true);
    }

    @Then("^a mensagem de erro deve ser exibida$")
    public void aMensagemDeErroDeveSerExibida() throws Throwable {
            Utils.waitForPageLoad(30,webdriver,Conditions.EXPECT_DOC_READY_STATE,
        Conditions.EXPECT_NO_SPINNERS,
        Conditions.EXPECT_NOT_WAITING,
        Conditions.EXPECT_RESPONSE_PROFESSOR);

        WebElement element = webdriver.findElement(By.id("resultProfessor"));
        Assert.assertTrue(element.getText().equals("Erro ao excluir professor: Professor possui associação com disciplina, e não pode ser deletado."));
    }
}
