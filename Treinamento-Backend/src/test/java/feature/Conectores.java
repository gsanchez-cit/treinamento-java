package feature;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

/**
 * Created by estagiocit on 01/03/2017.
 */
public class Conectores {

    public static WebDriver iniciaNavgador(){
        String pathEx = System.getProperty("user.dir");
        pathEx += "\\src\\test\\java\\driver\\cross.crx";

        ChromeOptions options = new ChromeOptions();
        options.addExtensions(new File(pathEx));
        options.addArguments("--start-maximized");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        String path = System.getProperty("user.dir");
        path += "\\src\\test\\java\\driver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver",path);
        return new ChromeDriver(capabilities);
    }
}
