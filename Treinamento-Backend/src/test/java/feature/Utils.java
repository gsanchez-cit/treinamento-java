package feature;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

/**
 * Created by estagiocit on 01/03/2017.
 */
public class Utils {

    public static boolean waitForPageLoad(int waitTimeInSec, WebDriver driver, ExpectedCondition<Boolean>... conditions) {
        boolean isLoaded = false;
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(waitTimeInSec, TimeUnit.SECONDS)
                .ignoring(StaleElementReferenceException.class)
                .pollingEvery(2, TimeUnit.SECONDS);
        for (ExpectedCondition<Boolean> condition : conditions) {
            isLoaded = wait.until(condition);
            if (isLoaded == false) {
//Stop checking on first condition returning false.
                break;
            }
        }
        return isLoaded;
    }
}
