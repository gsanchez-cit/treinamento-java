package feature;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by gsanchez on 08/09/2015.
 */
public class Conditions {

    /**
     * Returns 'true' if the value of the 'window.document.readyState' via
     * JavaScript is 'complete'
     */
    public static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
            Boolean result;
            try {
                result = ((JavascriptExecutor) driver).executeScript(script).equals("complete");
            } catch (Exception ex) {
                result = Boolean.FALSE;
            }
            return result;
        }
    };
    /**
     * Returns 'true' if there is no 'wait_dialog' element present on the page.
     */
    public static final ExpectedCondition<Boolean> EXPECT_NOT_WAITING = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = true;
            try {
                WebElement wait = driver.findElement(By.id("F"));
                if (wait.isDisplayed()) {
                    loaded = false;
                }
            } catch (StaleElementReferenceException serex) {
                loaded = false;
            } catch (NoSuchElementException nseex) {
                loaded = true;
            } catch (Exception ex) {
                loaded = false;
                System.out.println("EXPECTED_NOT_WAITING: UNEXPECTED EXCEPTION: " + ex.getMessage());
            }
            return loaded;
        }
    };

    /**
     * Returns true if there are no elements with the 'spinner' class name.
     */
    public static final ExpectedCondition<Boolean> EXPECT_NO_SPINNERS = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = true;
            try {
                List<WebElement> spinners = driver.findElements(By.className("spinner"));
                for (WebElement spinner : spinners) {
                    if (spinner.isDisplayed()) {
                        loaded = false;
                        break;
                    }
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

    /**
     * Returns verdadeiro se abriu tela de de pesquisa de cotação
     */
    public static final ExpectedCondition<Boolean> EXPECT_RESPONSE = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = false;
            try {
                WebElement element = driver.findElement(By.id("result"));
                if (element.isDisplayed()) {
                    loaded = true;
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

    public static final ExpectedCondition<Boolean> EXPECT_RESPONSE_PROFESSOR = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = false;
            try {
                WebElement element = driver.findElement(By.id("resultProfessor"));
                if (element.isDisplayed()) {
                    loaded = true;
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

    public static final ExpectedCondition<Boolean> EXPECT_RESPONSE_DISCIPLINA = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = false;
            try {
                WebElement element = driver.findElement(By.id("resultDisciplina"));
                if (element.isDisplayed()) {
                    loaded = true;
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

    public static final ExpectedCondition<Boolean> EXPECT_RESPONSE_TABLE = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = false;
            try {
                WebElement element = driver.findElement(By.id("linkstable"));
                if (element.isDisplayed()) {
                    loaded = true;
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

    public static final ExpectedCondition<Boolean> EXPECT_URL_UPDATE_PROFESSOR = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
            Boolean loaded = false;
            try {
                String url = driver.getCurrentUrl();
                if (url.contains("/updateProfessor/")) {
                    loaded = true;
                }
            } catch (Exception ex) {
                loaded = false;
            }
            return loaded;
        }
    };

}
