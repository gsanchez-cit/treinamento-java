package netgloo.controllers;

import netgloo.Excecoes.*;
import netgloo.Service.ProfessorService;
import netgloo.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by estagiocit on 20/02/2017.
 */
@Controller
@RequestMapping(value="/professor")
public class ProfessorController {

    @Autowired
    ProfessorService professorService;

    @RequestMapping(value="/create")
    @ResponseBody
    public ResponseEntity<String> create(@RequestBody ProfessorCreateForm professorCreateForm) {
        try {
            professorService.create(professorCreateForm);
        } catch (ProfessorJaCadastradoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (EnderecoInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (ProfessorInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body("Ok!");
    }

    @RequestMapping(value="/delete")
    @ResponseBody
    public ResponseEntity<String> delete(Long id){
        try{
            professorService.delete(id);
                return ResponseEntity.status(HttpStatus.OK).body(id.toString());
        } catch (ProfessorInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }catch (DataIntegrityViolationException e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Professor possui associação com disciplina, e não pode ser deletado.");
        }
    }

    @RequestMapping(value="/update")
    @ResponseBody
    public ResponseEntity<String> update (@RequestBody ProfessorUpdateForm professorUpdateForm) {
        try {
            professorService.update(professorUpdateForm);
        } catch (ProfessorInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (EnderecoInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body("ok");
    }

    @RequestMapping(value="/search")
    @ResponseBody
    public ResponseEntity<List<Professor>> search(@RequestBody ProfessorSearchForm professorSearchForm){
            return ResponseEntity.status(HttpStatus.OK).body(professorService.search(professorSearchForm));
    }

    @RequestMapping(value="/matricular")
    @ResponseBody
    public ResponseEntity<String> matricular(@RequestBody ProfessorDisciplinaKey professorDisciplinaKey) {
        try {
            professorService.matricular(professorDisciplinaKey);
            return ResponseEntity.status(HttpStatus.OK).body(professorDisciplinaKey.getIdDisciplina().toString());
        } catch (DisciplinaNaoExisteExeption e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (ProfessorNaoExisteException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (ProfessorInvalidoException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (DadosInvalidaException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }

    @RequestMapping(value="/desmatricular")
    @ResponseBody
    public ResponseEntity<String> desmatricular(@RequestBody ProfessorDisciplinaKey professorDisciplinaKey) {
        try {
            professorService.desmatricular(professorDisciplinaKey);
            return ResponseEntity.status(HttpStatus.OK).body(professorDisciplinaKey.getIdDisciplina().toString());
        } catch (MatriculaNaoExisteExeption e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (DisciplinaInvalidaException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (DadosInvalidaException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }
        @RequestMapping(value="/searchId")
        @ResponseBody
        public ResponseEntity<ProfessorReturnUpdateForm> searchById(Long id) {

            return ResponseEntity.status(HttpStatus.OK).body(professorService.getById(id));

        }

    @RequestMapping(value = "/listarTodasDisciplinas")
    @ResponseBody
    public ResponseEntity<List<ProfessorDisciplina>> listarDisciplinas(Long id){
        return ResponseEntity.status(HttpStatus.OK).body(professorService.listarDisciplinas(id));
    }

    @RequestMapping(value = "/listarDisciplinasDisponiveis")
    @ResponseBody
    public ResponseEntity<List<Disciplina>> listarDisciplinasDisponiveis(Long id){
        return ResponseEntity.status(HttpStatus.OK).body(professorService.listarDisciplinasDisponiveis(id));
    }
}
