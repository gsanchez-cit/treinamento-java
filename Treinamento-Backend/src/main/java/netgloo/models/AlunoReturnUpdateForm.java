package netgloo.models;

/**
 * Created by estagiocit on 01/03/2017.
 */
public class AlunoReturnUpdateForm {
    private Aluno aluno;
    private  Endereco endereco;

    public AlunoReturnUpdateForm() {
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
