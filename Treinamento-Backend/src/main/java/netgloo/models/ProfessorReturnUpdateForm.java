package netgloo.models;

/**
 * Created by estagiocit on 02/03/2017.
 */
public class ProfessorReturnUpdateForm {

    private Professor professor;
    private Endereco endereco;

    public ProfessorReturnUpdateForm() {
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
