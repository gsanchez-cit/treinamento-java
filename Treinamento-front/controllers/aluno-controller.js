
module.exports = {
    recuperaAluno: function (req,res,next) {
        var url = 'http://127.0.0.1:8080/aluno/searchId?id='+req.id;
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoRecuperaAluno,
            error: callbackErroRecuperaAluno
        });
    },

    callbackSucessoRecuperaAluno: function(data){
        console.log(data);
        $("#cpfAluno").val(data.aluno.cpf);
        $("#nomeAluno").val(data.aluno.nome);
        $("#datanascimentoAluno").val(data.aluno.dataNascimento);
        $("#sexoAluno").val(data.aluno.sexo);
        $("#emailAluno").val(data.aluno.email);
        if(data.endereco != null){
            $("#numeroAluno").val(data.endereco.numero);
            $("#logradouroAluno").val(data.endereco.logradouro);
            $("#cidadeAluno").val(data.endereco.cidade);
            $("#bairroAluno").val(data.endereco.bairro);
            $("#ufAluno").val(data.endereco.uf);
            $("#cepAluno").val(data.endereco.cep);
        }
        
    },

    callbackErroRecuperaAluno: function(data){
        var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
        console.log("erro: "+data.responseText);
        $('#result').append(result);
    }
}