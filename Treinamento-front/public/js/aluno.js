var aluno = {
    cadastrarAluno: function () {
        var url = 'http://127.0.0.1:8080/aluno/create';
        console.log("Entrou");
        var data = verificaEndereco();
        
        console.log(data);
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoCadastroAluno,
            error: callbackErroCadastroAluno
        });
        
    },

    pesquisa: function() {
        var url = 'http://127.0.0.1:8080/aluno/search';
        var data = verificaPesquisa();
        $("#ListaResult").empty();
        $('#containerResult').hide();
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoSearchAluno,
            error: callbackErroSearchAluno
        });
    },

    excluir: function(id) {
        var url = 'http://127.0.0.1:8080/aluno/delete?id='+id;
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoExcluirAluno,
            error: callbackErroExcluirAluno
        });
    },
    recuperaAluno: function () {
        var id = window.location.href.split("/");
        var url = 'http://127.0.0.1:8080/aluno/searchId?idAluno=' + id[id.length - 1];
        $.ajax({     
            type: "GET",
            contentType: "application/json",
            url: url,
            success: callbackSucessoRecuperaAluno,
            error: callbackErroRecuperaAluno
        });
    },
    updateAluno: function() {
        var url = 'http://127.0.0.1:8080/aluno/update';
        console.log("Entrou");
        var data = verificaEndereco();
        data.idAluno = $("#idAluno").val();
        
        console.log(data);
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoUpdateAluno,
            error: callbackErroUpdateAluno
        });
        
    }

}

var verificaEndereco = function () {
        
    if ( $("#logradouroAluno").val()!= "" || $("#numeroAluno").val() != ""
    || $("#cidadeAluno").val() != "" || $("#ufAluno").val() != ""
    || $("#cepAluno").val() != "" ) {
        return data = {
            cpf:$("#cpfAluno").val(),
            nome:$("#nomeAluno").val(),
            dataNascimento:$("#datanascimentoAluno").val(),
            sexo:$("#sexoAluno").val(),
            email:$("#emailAluno").val(),
            numero : $("#numeroAluno").val(),
            logradouro : $("#logradouroAluno").val(),
            cidade : $("#cidadeAluno").val(),
            bairro : $("#bairroAluno").val(),
            uf : $("#ufAluno").val(),
            cep : $("#cepAluno").val()
        }
    } else {
        return data = {
            cpf:$("#cpfAluno").val(),
            nome:$("#nomeAluno").val(),
            dataNascimento:$("#datanascimentoAluno").val(),
            sexo:$("#sexoAluno").val(),
            email:$("#emailAluno").val()
        }
    }

    
}

var verificaPesquisa = function() {

    data = {}
    if($("#nomeAluno").val()!= ""){
        data.nome = $("#nomeAluno").val();
    }

    if($("#datanascimentoAluno").val()!= ""){
        data.dataNascimento = $("#datanascimentoAluno").val();
    }

     if($("#sexoAluno").val()!= "s"){
        data.sexo = $("#sexoAluno").val();
    }
    return data;
}

var limparDados = function(){
    $("#cpfAluno").val("");
    $("#nomeAluno").val("");
    $("#datanascimentoAluno").val("");
    $("#sexoAluno").val("");
    $("#emailAluno").val("");
    $("#numeroAluno").val("");
    $("#logradouroAluno").val("");
    $("#cidadeAluno").val("");
    $("#bairroAluno").val("");
    $("#ufAluno").val("");
    $("#cepAluno").val("");
}


var callbackSucessoLivro = function(data){
    var result = '<div class="table-responsive"><table id="linkstable" class="table table-hover table-striped"><thead><tr><th>Nome</th><th>Descricao</th><th>Quantidade</th><th>Quantidade Emprestada</th><th>Excluir</th></tr></thead><tbdoy>';
    data.forEach(function (pesquisa) {
        result += '<tr><td>'+pesquisa.nome+'</td><td>'+pesquisa.descricao+'</td><td>'+pesquisa.quantidade+'</td><td>'+pesquisa.quantidadeEmprestada+'</td><><></tr>';
    });
    result += '</tbody></table></div>';
    $('#result').append(result);
    
    $('#linkstable').DataTable();
}

var callbackSucessoDropLivro = function(data){
    var livros = '';
     data.forEach(function (pesquisa) {
        livros +=  '<option value="'+pesquisa._id+'">'+pesquisa.nome+'</option>';
    });
    $('#livros').append(livros);

}

var callbackErroLivro = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao carregar Itens</div>';
    $('#result').append(result);
}

var callbackSucessoCadastroAluno = function(data){
    var result = '<div class="alert alert-success" role="alert">Item gravado com sucesso</div>'; 
    console.log(data);
    $('#result').append(result);
    limparDados();
}

var callbackErroCadastroAluno = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#result').append(result);
}

var callbackSucessoSearchAluno = function(data){
    var result = '<div class="table-responsive"><table id="linkstable" class="table table-hover table-striped"><thead><tr><th>Nome</th><th>Data de Nascimento</th><th>Sexo</th><th>E-mail</th></tr></thead><tbdoy>';
    data.forEach(function (pesquisa) {
        result += '<tr id='+pesquisa.idAluno+'><td><a href="updateAluno/'+pesquisa.idAluno+'">'+pesquisa.nome+'</a></td><td>'+ajustaData(new Date(pesquisa.dataNascimento))+'</td><td>'+pesquisa.sexo+'</td><td>'+pesquisa.email+'</td><td><a onclick=aluno.excluir('+pesquisa.idAluno+')>Excluir</a></td></tr>';
    });
    result += '</tbody></table></div>';
    $('#ListaResult').append(result);
    $('#containerResult').show();
    console.log(data);
}

var callbackErroSearchAluno = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#result').append(result);
}

var callbackSucessoExcluirAluno = function(data){
    $('#'+data).remove();
}

var callbackErroExcluirAluno = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#result').append(result);
}

var callbackSucessoRecuperaAluno = function(data){
    console.log(data);
    $("#idAluno").val(data.aluno.idAluno);
    $("#cpfAluno").val(data.aluno.cpf);
    $("#nomeAluno").val(data.aluno.nome);
    $("#datanascimentoAluno").val(ajustaDataForm(new Date(data.aluno.dataNascimento)));
    $("#sexoAluno").val(data.aluno.sexo);
    $("#emailAluno").val(data.aluno.email);
    if(data.endereco != null){
        $("#numeroAluno").val(data.endereco.numero);
        $("#logradouroAluno").val(data.endereco.logradouro);
        $("#cidadeAluno").val(data.endereco.cidade);
        $("#bairroAluno").val(data.endereco.bairro);
        $("#ufAluno").val(data.endereco.uf);
        $("#cepAluno").val(data.endereco.cep);
    }
    
};

var callbackErroRecuperaAluno = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#result').append(result);
};

var callbackSucessoUpdateAluno = function(data){
    var result = '<div class="alert alert-success" role="alert">Item gravado com sucesso</div>'; 
    console.log(data);
    $('#result').append(result);
}

var callbackErroUpdateAluno = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao gravar Item'+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#result').append(result);
}

var ajustaData = function(date){
    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

var ajustaDataForm = function(date){
    var currentDate = date.toISOString().slice(0,10);
    var currentTime = date.getHours() + ':' + date.getMinutes();

    return currentDate;
}
