var professor = {
    cadastrarProfessor: function () {
        var url = 'http://127.0.0.1:8080/professor/create';
        console.log("Entrou");
        var data = verificaEndereco();

        limparResults();
        
        console.log(data);
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoCadastroProfessor,
            error: callbackErroCadastroProfessor
        });
        
    },

    pesquisa: function() {
        var url = 'http://127.0.0.1:8080/professor/search';
        var data = verificaPesquisa();
        $("#ListaResult").empty();
        $('#containerResult').hide();
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoSearchProfessor,
            error: callbackErroSearchProfessor
        });
    },

    excluir: function(id) {
        var url = 'http://127.0.0.1:8080/professor/delete?id='+id;
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoExcluirProfessor,
            error: callbackErroExcluirProfessor
        });
    },
    recuperaProfessor: function () {
        var id = window.location.href.split("/");
        var url = 'http://127.0.0.1:8080/professor/searchId?id=' + id[id.length - 1];
        limparResults();
        $.ajax({     
            type: "GET",
            contentType: "application/json",
            url: url,
            success: callbackSucessoRecuperaProfessor,
            error: callbackErroRecuperaProfessor
        });
    },
    updateProfessor: function() {
        var url = 'http://127.0.0.1:8080/professor/update';
        console.log("Entrou");
        var data = verificaEndereco();
        data.idProfessor = $("#idProfessor").val();
        limparResults();
        console.log(data);
        $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoUpdateProfessor,
            error: callbackErroUpdateProfessor
        });
        
    },

    listaDisciplinas: function () {
        var id = window.location.href.split("/");
        var url = 'http://127.0.0.1:8080/professor/listarTodasDisciplinas?id=' + id[id.length - 1];
        $.ajax({     
            type: "GET",
            contentType: "application/json",
            url: url,
            success: callbackSucessoListaDisciplinas,
            error: callbackErroListaDisciplinas
        });
    },

    desmatricular: function(disciplina, professor) {
        var url = 'http://127.0.0.1:8080/professor/desmatricular';
        var data = {
            idProfessor: professor,
            idDisciplina: disciplina
        };
        limparResults();
          $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoDesmatricular,
            error: callbackErroDesmatricular
        });
        
    },

    listaDisciplinasDisponiveis: function() {
        var id = window.location.href.split("/");
        var url = 'http://127.0.0.1:8080/professor/listarDisciplinasDisponiveis?id='+ id[id.length - 1];
    $.ajax({     
            type: "GET",
            contentType: "application/json",
            url: url,
            success: callbackSucessoListarDisponiveis,
            error: callbackErroListarDisponiveis
        });
    },
    matricularProfessor: function() {
        var id = window.location.href.split("/");
        var url = 'http://127.0.0.1:8080/professor/matricular';
        var data = {
            idProfessor: id[id.length - 1],
            idDisciplina: $("#disciplinasMatricular").val()
        };
        limparResults();
          $.ajax({     
            type: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(data),
            success: callbackSucessoMatricular,
            error: callbackErroMatricular
        });
        
    }


}

var limparResults = function(){
    $("#resultProfessor").empty();
    $("#resultDisciplina").empty();
}

var verificaEndereco = function () {
        
    if ( $("#logradouroProfessor").val()!= "" || $("#numeroProfessor").val() != ""
    || $("#cidadeProfessor").val() != "" || $("#ufProfessor").val() != ""
    || $("#cepProfessor").val() != "" ) {
        return data = {
            cpf:$("#cpfProfessor").val(),
            nome:$("#nomeProfessor").val(),
            dataNascimento:$("#datanascimentoProfessor").val(),
            sexo:$("#sexoProfessor").val(),
            email:$("#emailProfessor").val(),
            numero : $("#numeroProfessor").val(),
            logradouro : $("#logradouroProfessor").val(),
            cidade : $("#cidadeProfessor").val(),
            bairro : $("#bairroProfessor").val(),
            uf : $("#ufProfessor").val(),
            cep : $("#cepProfessor").val(),
            biografia: $("#biografia").val()
        }
    } else {
        return data = {
            cpf:$("#cpfProfessor").val(),
            nome:$("#nomeProfessor").val(),
            dataNascimento:$("#datanascimentoProfessor").val(),
            sexo:$("#sexoProfessor").val(),
            email:$("#emailProfessor").val(),
            biografia: $("#biografia").val()
        }
    }

    
}

var verificaPesquisa = function() {

    data = {}
    if($("#nomeProfessor").val()!= ""){
        data.nome = $("#nomeProfessor").val();
    }

    if($("#datanascimentoProfessor").val()!= ""){
        data.dataNascimento = $("#datanascimentoProfessor").val();
    }

     if($("#sexoProfessor").val()!= "s"){
        data.sexo = $("#sexoProfessor").val();
    }
    return data;
}

var limparDados = function(){
    $("#cpfProfessor").val("");
    $("#nomeProfessor").val("");
    $("#datanascimentoProfessor").val("");
    $("#sexoProfessor").val("");
    $("#emailProfessor").val("");
    $("#numeroProfessor").val("");
    $("#logradouroProfessor").val("");
    $("#cidadeProfessor").val("");
    $("#bairroProfessor").val("");
    $("#ufProfessor").val("");
    $("#cepProfessor").val("");
    $("#biografia").val("");
}

var callbackSucessoCadastroProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-success" role="alert">Professor salvo com sucesso</div>'; 
    console.log(data);
    $('#resultProfessor').append(result);
    limparDados();
}

var callbackErroCadastroProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao gravar Professor: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}

var callbackSucessoSearchProfessor = function(data){
    var result = '<div class="table-responsive"><table id="linkstable" class="table table-hover table-striped"><thead><tr><th>Nome</th><th>Data de Nascimento</th><th>Sexo</th><th>E-mail</th></tr></thead><tbdoy>';
    data.forEach(function (pesquisa) {
        result += '<tr id='+pesquisa.id+'><td><a href="updateProfessor/'+pesquisa.id+'">'+pesquisa.nome+'</a></td><td>'+ajustaData(new Date(pesquisa.dataNascimento))+'</td><td>'+pesquisa.sexo+'</td><td>'+pesquisa.email+'</td><td><a onclick=professor.excluir('+pesquisa.id+')>Excluir</a></td></tr>';
    });
    result += '</tbody></table></div>';
    $('#ListaResult').append(result);
    $('#containerResult').show();
    console.log(data);
}

var callbackErroSearchProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao recuperar professores: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}

var callbackSucessoExcluirProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-success" role="alert">Professor removido com sucesso.</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
    
    $('#'+data).remove();
}

var callbackErroExcluirProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao excluir professor: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}

var callbackSucessoRecuperaProfessor = function(data){
    console.log(data);
    $("#idProfessor").val(data.professor.id);
    $("#cpfProfessor").val(data.professor.cpf);
    $("#nomeProfessor").val(data.professor.nome);
    $("#datanascimentoProfessor").val(ajustaDataForm(new Date(data.professor.dataNascimento)));
    $("#sexoProfessor").val(data.professor.sexo);
    $("#emailProfessor").val(data.professor.email);
    $("#biografia").val(data.professor.biografia);
    if(data.endereco != null){
        $("#numeroProfessor").val(data.endereco.numero);
        $("#logradouroProfessor").val(data.endereco.logradouro);
        $("#cidadeProfessor").val(data.endereco.cidade);
        $("#bairroProfessor").val(data.endereco.bairro);
        $("#ufProfessor").val(data.endereco.uf);
        $("#cepProfessor").val(data.endereco.cep);
    }
    
};

var callbackErroRecuperaProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao recuperar Professor: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
};

var callbackSucessoUpdateProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-success" role="alert">Professor atualizado com sucesso</div>'; 
    console.log(data);
    $('#resultProfessor').append(result);
}

var callbackErroUpdateProfessor = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao atualizar professor: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}

var callbackSucessoListaDisciplinas = function(data){
    var result = '<div class="table-responsive"><table id="linkstable" class="table table-hover table-striped"><thead><tr><th>Disciplina</th><th>Código</th><th>Excluir</th></tr></thead><tbdoy>';
    data.forEach(function (pesquisa) {
        result += '<tr id="cadastrado_'+pesquisa.disciplina.idDisciplina+'"><td>'+pesquisa.disciplina.nome+'</td><td>'+pesquisa.disciplina.codigo+'</td><td><a onclick=professor.desmatricular('+pesquisa.disciplina.idDisciplina+','+pesquisa.professor.id+')>Excluir</a></td></tr>';
    });
    result += '</tbody></table></div>';
    $('#disciplinas').append(result);
    console.log(data);
}

var callbackErroListaDisciplinas = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro carregar lista de disciplinas: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}


var callbackSucessoDesmatricular = function(data){
    var result = '<div id="resultMessage"  class="alert alert-success" role="alert">professor desmatriculado com sucesso</div>'; 
    console.log(data);
    $('#cadastrado_' + data).remove();
    $('#disciplinasMatricular').empty();
    professor.listaDisciplinasDisponiveis();
    $('#resultProfessor').append(result);
}

var callbackErroDesmatricular = function(data){
    var result = '<div id="resultMessage"  class="alert alert-danger" role="alert">Erro ao desmatricular professor:  '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultProfessor').append(result);
}

var callbackSucessoListarDisponiveis = function(data){
    var result = "";
    if(data.length > 0){
        data.forEach(function (pesquisa) {
            result += '<option id="disponivel_'+pesquisa.idDisciplina+'" value="'+pesquisa.idDisciplina+'"> '+ pesquisa.nome+'-'+ pesquisa.codigo+'</option>';
        });
        $("#matriculaProfessor").prop('disabled',false);
    } else {
        $("#matriculaProfessor").prop('disabled',true);
    }
    
    $('#disciplinasMatricular').append(result);
    console.log(data);
}

var callbackErroListarDisponiveis = function(data){
    var result = '<div id="resultMessage" class="alert alert-danger" role="alert">Erro ao carregar disciplinas disponiveis: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultDisciplina').append(result);
}

var callbackSucessoMatricular = function(data){
    var result = '<div id="resultMessage"  class="alert alert-success" role="alert">professor matriculado com sucesso</div>'; 
    $("#disciplinas").empty();
    $("#disponivel_" + data).remove();
    professor.listaDisciplinas();
    $("#resultDisciplina").append(result);
}

var callbackErroMatricular = function(data){
    var result = '<div class="alert alert-danger" role="alert">Erro ao matricular professor: '+data.responseText+'</div>';
    console.log("erro: "+data.responseText);
    $('#resultDisciplina').append(result);
}


var ajustaData = function(date){
    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

var ajustaDataForm = function(date){
    var currentDate = date.toISOString().slice(0,10);
    var currentTime = date.getHours() + ':' + date.getMinutes();

    return currentDate;
}
